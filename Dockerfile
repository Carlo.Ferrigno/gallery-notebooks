# For finding latest versions of the base image see
# https://github.com/SwissDataScienceCenter/renkulab-docker
ARG RENKU_BASE_IMAGE=renku/renkulab-py:3.9-0.13.1
FROM ${RENKU_BASE_IMAGE}

# Uncomment and adapt if code is to be included in the image
# COPY src /code/src

# Uncomment and adapt if your R or python packages require extra linux (ubuntu) software
# e.g. the following installs apt-utils and vim; each pkg on its own line, all lines
# except for the last end with backslash '\' to continue the RUN line
#
USER root
RUN apt-get update && \
   apt-get install -y --no-install-recommends \
   apt-utils \
   vim libatlas-base-dev libatlas3-base libfftw3-dev autoconf libtool git ca-certificates \
   nodejs python3-novnc tightvncserver libvncserver1 automake libcfitsio-dev


# install the python dependencies
COPY requirements.txt  /tmp/
RUN pip3 install pip --upgrade
RUN pip install future Cython
RUN pip install -r /tmp/requirements.txt --src /src --upgrade


# RENKU_VERSION determines the version of the renku CLI
# that will be used in this image. To find the latest version,
# visit https://pypi.org/project/renku/#history.
ARG RENKU_VERSION=2.3.2
RUN pip install renku==${RENKU_VERSION}

#sextractor
RUN cd /tmp && \
        wget https://github.com/astromatic/sextractor/archive/refs/tags/2.28.0.tar.gz && \
        tar xfz 2.28.0.tar.gz && \
        rm 2.28.0.tar.gz && \
        cd /tmp/sextractor-2.28.0 && \
        sh autogen.sh && \
        echo $LD_LIBRARY_PATH && \
        ./configure && \
        make -j && \
        make install


RUN mkdir -p /home/jovyan/my-notebooks && \
    chown jovyan.users /home/jovyan/my-notebooks
USER ${NB_USER}

# This will not work on renkulab
ADD *.ipynb /home/jovyan/my-notebooks/
ADD renku_init.sh /home/jovyan/my-notebooks/
RUN source /home/jovyan/my-notebooks/renku_init.sh
ADD .oda-token /home/jovyan/.oda-token
                                                                                                    


# ########################################################
# # Do not edit this section and do not add anything below

# # Install renku from pypi or from github if it's a dev version
# RUN if [ -n "$RENKU_VERSION" ] ; then \
#         # source .renku/venv/bin/activate ; \
#         echo -e "\033[31m$PYTHONPATH $PWD\033[0m" ; \
#         currentversion=$(renku --version) ; \
#         if [ "$RENKU_VERSION" != "$currentversion" ] ; then \
#             pip uninstall renku -y ; \
#             gitversion=$(echo "$RENKU_VERSION" | sed -n "s/^[[:digit:]]\+\.[[:digit:]]\+\.[[:digit:]]\+\(rc[[:digit:]]\+\)*\(\.dev[[:digit:]]\+\)*\(+g\([a-f0-9]\+\)\)*\(+dirty\)*$/\4/p") ; \
#             if [ -n "$gitversion" ] ; then \
#                 pip install --force "git+https://github.com/SwissDataScienceCenter/renku-python.git@$gitversion" --src /src ;\
#             else \
#                 pip install renku==${RENKU_VERSION} --src /src ;\
#             fi \
#         fi \
#     fi

# ########################################################

RUN renku --version
