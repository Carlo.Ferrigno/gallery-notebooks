#!/bin/bash
#SBATCH --partition=p3
#SBATCH -D /scratch/ferrigno/INTEGRAL/gallery-notebooks


ml load GCCcore/8.2.0 Singularity/3.4.0-Go-1.12 
cd /scratch/ferrigno/INTEGRAL/gallery-notebooks 
#make parametrized-rev-current singularity-process-nrt-revs-cwd-folder
#FORCE_REPROCESSING=True N_START=2602 N_STOP=2603 make parametrized-process-cons-rev singularity-process-cons-revs-cwd-folder
#FORCE_REPROCESSING=True N_START=128 N_STOP=129 make parametrized-process-cons-rev singularity-process-cons-revs-cwd-folder
#make parametrized-rev-current singularity-process-nrt-revs-cwd-folder
NBARGS_PYDICT="{rev_num_indicator: 2619, data_version: NRT, use_jemx2: True, notebooks_folder: $PWD}" make singularity-process-nrt-revs-cwd-folder

