Using a jupyter notebook

You need to get the ODA token using instructions at
https://oda-api.readthedocs.io/en/latest/user_guide/Authentication.html
and save it into 
${HOME}/.oda-token

Then, issue
export ODA_TOKEN=`cat ${HOME}/.oda-token`

To run the notebook:
Install docker

make build

JUPYTER_PORT=8888 make run

(use your preferred value for the jupyter port) and one the given address in your browser


## Revolution mosaics (both NRT and CONS data)

It will extract mosaics for each observations of a specific revolution, or the current revolution or previous ones (see `rev_num` parameter)

```bash
JUPYTER_PORT=8888 make rev-mosaics 
```

This command runs the notebook "Generic Revolution Mosaics.ipynb" as an application, useful for a service
the following parameters can be passed, the default values are shown:
```python
rev_num = 0
#If <=0 it will take the current revolution (zero) or previous ones, select NRT as data_version in this case,otherwise the revolution number
E1_keV = "28.0"
E2_keV = "40.0"
J_E1_keV = "3.0"
J_E2_keV = "20.0"
osa_version='OSA11.2'
detection_threshold=7
host_type='staging'
to_exclude_from_title='' 
#if this string is in the title of the observation, this is excluded from the analysis. Leave '' to not use it.
use_isgri=True
use_jemx1=True
use_jemx2=True
include_new_sources = False
data_version = 'CONS' #It can be CONS or NRT
```

example: 
```bash
JUPYTER_PORT=1234 NBARGS_PYDICT='{\"rev_num\" : 0, \"data_version\" : \"NRT\", \"use_jemx2\": False}' make rev-mosaics
```
will extract and upload to the gallery the current revolution mosaics (Note the NRT parameter)

```bash
NBARGS_PYDICT='{\"rev_num\" : 2400, \"data_version\" : \"CONS\"}' JUPYTER_PORT=8888 make rev-mosaics 
```
will extract and upload to the gallery the revolution 2400 mosaics (CONS is default)


## Revolution spectra (Based on revolution Mosaics)

It will extract spectra for each observation and source detected by revolution mosaics.
It runs for the current revolution or previous ones (see rev_num parameter)

```bash
JUPYTER_PORT=8888 make rev-spectra
```

This command runs the notebook "Generic Revolution Spectra.ipynb" as an application, useful for a service
the following parameters can be passed, the default values are shown:
```python
rev_num = 2481
use_isgri = True
use_jemx1 = True
use_jemx2 = False
E1_keV = "28.0"
E2_keV = "40.0"
J_E1_keV = "3.0"
J_E2_keV = "20.0"
osa_version = 'OSA11.2'
detection_threshold = 7
host_type = 'staging'
to_exclude_from_title = '' 
isgri_grouping = [25,250,-30]
isgri_systematic_fraction = 0.015
jemx_systematic_fraction = 0.05
```

```bash
NBARGS_PYDICT='{\"rev_num\" : 2401}' JUPYTER_PORT=8888 make rev-spectra
```

## Images
Cleaning with sextractor the images
jemx-full-image-sextractor.ipynb
isgri-full-image-sextractor.ipynb


## Light curves

It will extract a light curve for a specific source 
### ISGRI

```bash
JUPYTER_PORT=8888 make isgri-lc
```

This command runs the notebook "isgri-lightcurve.ipynb" as an application, useful for a service
the following parameters can be passed, the default values are shown:

```python
tstart='2003-02-01T00:00:00'
tstop='2023-03-31T23:59:59'
radius=10
source_name="GX 1+4"

osa_version="OSA11.2"
data_version='cons'
integral_data_rights='all-private'
s_max=500

E1_keV=28.
E2_keV=50.
#for image and catalog extraction
detection_threshold=7.0

#run_mosaic=True
make_image=True
make_lc=True
lc_time_bin=4000

#We sample the science window list to make a mosaic to avoid a very time consuming step to extract a catalog
n_scw_image=50
systematic_fraction = 0 #To be added to plots
```
* Example: 

```bash
NBARGS_PYDICT='{\"source_name\" : \"GX 5-1\", \"data_version\" : \"CONS\"}' JUPYTER_PORT=1234 make isgri-lc
```
### JEM-X
```bash
JUPYTER_PORT=8888 make jemx-lc
```

This command runs the notebook "jemx-lightcurve.ipynb" as an application, useful for a service
the following parameters can be passed, the default values are shown:

```python
#General search
tstart='2021-02-01T00:00:00'
tstop='2021-02-27T23:59:59'
radius=3.5
source_name="GX 1+4"
jemx_unit=1

osa_version="OSA11.2"
data_version='cons'
integral_data_rights='all-private'

#### NB
# This is the limit for each call (to be raised to 500)
s_max=500

#For image and light curve
E1_keV=3.
E2_keV=20.
#for image and catalog extraction
detection_threshold=7.0

make_lc=True
make_image=True
lc_time_bin=1000

#We sample the science window list to make a mosaic to avoid a very time consuming step to extract a catalog
n_scw_image=10
systematic_fraction = 0

host_type='staging'
```
* Example: 

```bash
NBARGS_PYDICT='{\"source_name\" : \"GX 5-1\", \"data_version\" : \"CONS\"}' JUPYTER_PORT=1234 make jemx-lc
```

## Spectra

It will extract a spectrum for a specific source 
### ISGRI

```bash
JUPYTER_PORT=8888 make isgri-spec
```

This command runs the notebook "isgri-spectrum.ipynb" as an application, useful for a service
the following parameters can be passed, the default values are shown:

```python
#General search
tstart='2021-02-01T00:00:00'
tstop='2021-03-31T23:59:59'
#I use 3.5 deg for JEM-X and 12 for ISGRI, by running twice the notebook.
radius=10
source_name="Crab"

osa_version="OSA11.2"
data_version='cons'
integral_data_rights='all-private'

# This is the limit for each call (to be raised to 500)
s_max=500

#For ISGRI image and light curve
E1_keV=20.
E2_keV=80.
#for image and catalog extraction
detection_threshold=7.0

#ISGRI
run_spec=True
make_image=False

#We sample the science window list to make a mosaic to avoid a very time consuming step to extract a catalog
n_scw_image=5
systematic_fraction = 0.015

api_catalog_file = ''
host_type='staging'
```
* Example: 

```bash
NBARGS_PYDICT='{\"source_name\" : \"GX 5-1\", \"data_version\" : \"CONS\"}' JUPYTER_PORT=1234 make isgri-spec
```
### JEM-X
```bash
JUPYTER_PORT=8888 make jemx-spec
```

This command runs the notebook "jemx-spectrum.ipynb" as an application, useful for a service
the following parameters can be passed, the default values are shown:

```python
#General search
tstart='2021-02-01T00:00:00'
tstop='2021-02-27T23:59:59'
#I use 3.5 deg for JEM-X and 12 for ISGRI, by running twice the notebook.
radius=3.5
source_name="GX 1+4"
jemx_unit=1

osa_version="OSA11.2"
data_version='cons'
integral_data_rights='all-private'

#### NB
# This is the limit for each call (to be raised to 500)
s_max=500

#For image and light curve
E1_keV=3.
E2_keV=20.
#for image and catalog extraction
detection_threshold=7.0

run_spec=True
make_image=True

#We sample the science window list to make a mosaic to avoid a very time consuming step to extract a catalog
n_scw_image=10
systematic_fraction = 0.05

api_catalog_file = ''
host_type='staging'
```
* Example: 

```bash
NBARGS_PYDICT='{\"source_name\" : \"GX 5-1\", \"data_version\" : \"CONS\"}' JUPYTER_PORT=1234 make jemx-spec
```

# Running notebooks with singularity

## CONS
For processing the latest *five* revelutions extracted from the CONS table:
```bash
make parametrized-process-cons-rev-minus-1 singularity-process-cons-revs-cwd-folder
```
For processing a spcific range of revelutions extracted from the CONS table:
```bash
make N_START=100 N_STOP=110 parametrized-process-cons-rev singularity-process-cons-revs-cwd-folder
```
Note that a file named .secret.yaml should be in the running folder to download https://www.isdc.unige.ch/integral/restricted/Operations/Shift/Status/consolidated.html in the notebook ProcessConsRevolutions.ipynb

## NRT
For processing instead the latest NRT data, more specifically from the latest revolution:
```bash
make parametrized-rev-current singularity-process-nrt-revs-cwd-folder
```


# Running a notebook on LESTA and control it from your laptop

- Make a tunnel to lesta from your laptop (with your username)
`ssh -4 -t -L1222:localhost:1222 ferrigno@login02 ssh -4 -L1222:localhost:1222 -t ferrigno@lesta02`
- Once you log in, enable the singularity module
`. /usr/local/etc/setup-modules.sh && ml load GCCcore/8.2.0 Singularity/3.4.0-Go-1.12`
- Upload your sif image (it was built with `make build-no-cache;make singularity`
- Then start your jupyter kernel
`SIF_VOLUME_FILE=carloferrigno_mmoda-gallery_1.0.4.sif JUPYTER_PORT=1222 make sing-run`
- From your laptop, open a browser wth the address that appears on the screen

# Running a bunch of CONS Processing

`E1_keV=20 N_START=47 N_STOP=51 make parametrized-process-cons-rev singularity-process-cons-revs`


# Other notebooks are still WIP
