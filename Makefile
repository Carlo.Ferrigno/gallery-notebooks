
IMAGE?=carloferrigno/mmoda-gallery:1.0.4
IMAGE_LATEST?=carloferrigno/mmoda-gallery:latest
SIF_VOLUME_FILE?=carloferrigno_mmoda-gallery_1.0.4.sif
DUSER := $(shell id -u)
ODA_TOKEN?=$(shell cat .oda-token)
JUPYTER_PORT?=8889
N_START?=0
N_STOP?=-1
FORCE_REPROCESSING?=False
E1_keV?="28.0"
REV_NUM_INDICATOR?=0
NOTEBOOK_FOLDER?=$(PWD)
time := $(shell date --utc +'%Y-%m-%dT%H:%M:%S')

parametrized-rev-current:
	$(eval override NBARGS_PYDICT = {\"rev_num_indicator\": 0, \"data_version\": \"NRT\", \"use_jemx2\": False, \"notebooks_folder\": \"$(NOTEBOOK_FOLDER)\"})
parametrized-rev-minus-1:
	$(eval override NBARGS_PYDICT = {\"rev_num_indicator\": -1, \"data_version\": \"NRT\", \"use_jemx2\": False, \"notebooks_folder\": \"$(NOTEBOOK_FOLDER)\"})
parametrized-rev-minus-2:
	$(eval override NBARGS_PYDICT = {\"rev_num_indicator\": -2, \"data_version\": \"NRT\", \"use_jemx2\": False, \"notebooks_folder\": \"$(NOTEBOOK_FOLDER)\"})
parametrized-rev-minus-indicator:
	$(eval override NBARGS_PYDICT = {\"rev_num_indicator\": $(REV_NUM_INDICATOR), \"data_version\": \"NRT\", \"use_jemx2\": False, \"notebooks_folder\": \"$(NOTEBOOK_FOLDER)\"})
	@echo $(NBARGS_PYDICT)
parametrized-process-cons-rev-minus-1:
	$(eval override NBARGS_PYDICT = {\"n_start\": -1, \"E1_keV\" : $(E1_keV), \"notebooks_folder\": \"$(NOTEBOOK_FOLDER)\"})
parametrized-process-cons-rev:
	$(eval override NBARGS_PYDICT = {\"E1_keV\" : $(E1_keV), \"n_start\": $(N_START), \"n_stop\": $(N_STOP), \"force_reprocessing\": $(FORCE_REPROCESSING), \"notebooks_folder\": \"$(NOTEBOOK_FOLDER)\"})
	@echo $(NBARGS_PYDICT)
		
enable-modules:
	. /usr/local/etc/setup-modules.sh && ml load GCCcore/8.2.0 Singularity/3.4.0-Go-1.12
NBARGS_PYDICT?=''

push: build
	docker push $(IMAGE) 
	docker push $(IMAGE_LATEST) 

build: Dockerfile
	docker build . -t $(IMAGE)
	docker build . -t $(IMAGE_LATEST)

build-no-cache:
	docker build . --no-cache -t $(IMAGE)
	docker build . --no-cache -t $(IMAGE_LATEST)

pull:
	docker pull $(IMAGE) 
	docker pull $(IMAGE_LATEST) 

test: build
	docker run -p ${JUPYTER_PORT}:${JUPYTER_PORT} -e HOME_OVERRRIDE=/tmp-home -v $(PWD):/tmp-home -it $(IMAGE_LATEST) bash -c 'source /usr/local/bin/init.sh; bash $$HOME/self-test.sh'
 
run: 
	docker run -e ODA_TOKEN=${ODA_TOKEN} -v $(PWD):/home/jovyan -it --entrypoint='' -p $(JUPYTER_PORT):$(JUPYTER_PORT) --user $(DUSER)  $(IMAGE_LATEST) bash -c "jupyter notebook --ip 0.0.0.0 --no-browser --port=$(JUPYTER_PORT)"

sing-run: enable-modules
	singularity exec -B /isdc:/isdc -B $(PWD):/home/jovyan  $(SIF_VOLUME_FILE) bash -c "jupyter notebook --ip 0.0.0.0 --no-browser --port=$(JUPYTER_PORT)"

renkurun:
	docker run -p ${JUPYTER_PORT}:${JUPYTER_PORT} -it --entrypoint='' $(IMAGE_LATEST) bash

renkujupyterlab: 
	docker run -e NBARGS_PYDICT="$(NBARGS_PYDICT)" -e ODA_TOKEN="${ODA_TOKEN}" -v $(PWD):/home/jovyan -it --entrypoint='' -p ${JUPYTER_PORT}:${JUPYTER_PORT} --user $(DUSER)  $(IMAGE_LATEST) bash -c "jupyter lab --no-browser --port=$(JUPYTER_PORT)"

rev-mosaics:
	docker run -e NBARGS_PYDICT="$(NBARGS_PYDICT)" -e ODA_TOKEN="${ODA_TOKEN}" -v $(PWD):/home/jovyan --entrypoint='' -p $(JUPYTER_PORT):$(JUPYTER_PORT) --user $(DUSER)  $(IMAGE_LATEST) bash -c "echo \"${NBARGS_PYDICT}\" > nbargs.yaml; cat nbargs.yaml; papermill --parameters_file nbargs.yaml Generic\ Revolution\ Mosaics.ipynb out/Generic\ Revolution\ Mosaics_output_$(time).ipynb --log-output"
sing-rev-mosaics: enable-modules
	singularity exec -B /isdc:/isdc -B $(PWD):/home/jovyan $(SIF_VOLUME_FILE) bash -c "cd /home/jovyan; echo \"${NBARGS_PYDICT}\" > nbargs.yaml; cat nbargs.yaml; papermill --parameters_file nbargs.yaml Generic\ Revolution\ Mosaics.ipynb out/Generic\ Revolution\ Mosaics_output_$(time).ipynb --log-output" >> /home/astro/barni/gallery-notebooks/log/mosaics_log_$(time).log 2>&1

rev-spectra: 
	docker run -e NBARGS_PYDICT="$(NBARGS_PYDICT)" -e ODA_TOKEN="${ODA_TOKEN}" -v $(PWD):/home/jovyan  --entrypoint='' -p $(JUPYTER_PORT):$(JUPYTER_PORT) --user $(DUSER)  $(IMAGE_LATEST) bash -c "echo \"${NBARGS_PYDICT}\" > nbargs.yaml; cat nbargs.yaml; papermill --parameters_file nbargs.yaml Generic\ Revolution\ Spectra.ipynb out/Generic\ Revolution\ Spectra_output_$(time).ipynb --log-output"
sing-rev-spectra: enable-modules
	singularity exec -B /isdc:/isdc -B $(PWD):/home/jovyan $(SIF_VOLUME_FILE) bash -c "cd /home/jovyan; echo \"${NBARGS_PYDICT}\" > nbargs.yaml; cat nbargs.yaml; papermill --parameters_file nbargs.yaml Generic\ Revolution\ Spectra.ipynb out/Generic\ Revolution\ Spectra_output_$(time).ipynb --log-output" >> /home/astro/barni/gallery-notebooks/log/spectra_log_$(time).log 2>&1

rev-lc: 
	docker run -e NBARGS_PYDICT="$(NBARGS_PYDICT)" -e ODA_TOKEN="${ODA_TOKEN}" -v $(PWD):/home/jovyan  --entrypoint='' -p $(JUPYTER_PORT):$(JUPYTER_PORT) --user $(DUSER)  $(IMAGE_LATEST) bash -c "echo \"${NBARGS_PYDICT}\" > nbargs.yaml; cat nbargs.yaml; papermill --parameters_file nbargs.yaml Generic\ Revolution\ LC.ipynb out/Generic\ Revolution\ LC_output_$(time).ipynb --log-output"
sing-rev-lc: enable-modules
	singularity exec -B /isdc:/isdc -B $(PWD):/home/jovyan $(SIF_VOLUME_FILE) bash -c "cd /home/jovyan; echo \"${NBARGS_PYDICT}\" > nbargs.yaml; cat nbargs.yaml; papermill --parameters_file nbargs.yaml Generic\ Revolution\ LC.ipynb out/Generic\ Revolution\ LC_output_$(time).ipynb --log-output" >> /home/astro/barni/gallery-notebooks/log/lc_log_$(time).log 2>&1

singularity-process-cons-revs: enable-modules
	singularity exec -B /isdc:/isdc -B $(PWD):/home/jovyan $(SIF_VOLUME_FILE) bash -c "cd /home/jovyan; echo \"${NBARGS_PYDICT}\" > nbargs.yaml; cat nbargs.yaml; papermill --parameters_file nbargs.yaml ProcessConsRevolutions.ipynb out/ProcessConsRevolutions_output_$(time).ipynb --log-output" >> log/ProcessConsRevolutions_log_$(time).log 2>&1
singularity-process-cons-revs-cwd-folder: enable-modules
	singularity exec -B /isdc:/isdc -B $(PWD):/home/jovyan $(SIF_VOLUME_FILE) bash -c "cd /home/jovyan; echo \"${NBARGS_PYDICT}\" > nbargs.yaml; cat nbargs.yaml; mkdir output_folder_ProcessConsRevolutions_$(time); echo $(ODA_TOKEN) > output_folder_ProcessConsRevolutions_$(time)/.oda-token; papermill --cwd output_folder_ProcessConsRevolutions_$(time) --parameters_file nbargs.yaml ProcessConsRevolutions.ipynb out/ProcessConsRevolutions_output_$(time).ipynb --log-output" >> log/ProcessConsRevolutions_log_$(time).log 2>&1; tail -50 log/ProcessConsRevolutions_log_$(time).log; grep -i space log/ProcessConsRevolutions_log_$(time).log

singularity-process-nrt-revs: enable-modules
	singularity exec -B /isdc:/isdc -B $(PWD):/home/jovyan $(SIF_VOLUME_FILE) bash -c "cd /home/jovyan; echo \"${NBARGS_PYDICT}\" > nbargs.yaml; cat nbargs.yaml; papermill --parameters_file nbargs.yaml ProcessNRTRevolutions.ipynb out/ProcessNRTRevolutions_output_$(time).ipynb --log-output" >> log/ProcessNRTRevolutions_log_$(time).log 2>&1
singularity-process-nrt-revs-cwd-folder: enable-modules
	singularity exec -B /isdc:/isdc -B $(PWD):/home/jovyan $(SIF_VOLUME_FILE) bash -c "cd /home/jovyan; echo \"${NBARGS_PYDICT}\" > nbargs.yaml; cat nbargs.yaml; mkdir output_folder_ProcessNRTRevolutions_$(time); echo $(ODA_TOKEN) > output_folder_ProcessNRTRevolutions_$(time)/.oda-token; papermill --cwd output_folder_ProcessNRTRevolutions_$(time) --parameters_file nbargs.yaml ProcessNRTRevolutions.ipynb out/ProcessNRTRevolutions_output_$(time).ipynb --log-output" >> log/ProcessNRTRevolutions_log_$(time).log 2>&1; tail -50 log/ProcessNRTRevolutions_log_$(time).log; grep -i space log/ProcessNRTRevolutions_log_$(time).log 

isgri-lc:
	docker run -e NBARGS_PYDICT="$(NBARGS_PYDICT)" -e ODA_TOKEN="${ODA_TOKEN}" -v $(PWD):/home/jovyan -it --entrypoint='' -p $(JUPYTER_PORT):$(JUPYTER_PORT) --user $(DUSER)  $(IMAGE_LATEST) bash -c "echo \"${NBARGS_PYDICT}\" > nbargs.yaml; cat nbargs.yaml; papermill --parameters_file nbargs.yaml isgri-lightcurve.ipynb out/isgri-lightcurve_output_$(time).ipynb --log-output"

isgri-spec:
	docker run -e NBARGS_PYDICT="$(NBARGS_PYDICT)" -e ODA_TOKEN="${ODA_TOKEN}" -v $(PWD):/home/jovyan -it --entrypoint='' -p $(JUPYTER_PORT):$(JUPYTER_PORT) --user $(DUSER)  $(IMAGE_LATEST) bash -c "echo \"${NBARGS_PYDICT}\" > nbargs.yaml; cat nbargs.yaml; papermill --parameters_file nbargs.yaml isgri-spectrum.ipynb out/isgri-spectrum_output_$(time).ipynb --log-output"

jemx-lc:
	docker run -e NBARGS_PYDICT="$(NBARGS_PYDICT)" -e ODA_TOKEN="${ODA_TOKEN}" -v $(PWD):/home/jovyan -it --entrypoint='' -p $(JUPYTER_PORT):$(JUPYTER_PORT) --user $(DUSER)  $(IMAGE_LATEST) bash -c "echo \"${NBARGS_PYDICT}\" > nbargs.yaml; cat nbargs.yaml; papermill --parameters_file nbargs.yaml jemx-lightcurve.ipynb out/jemx-lightcurve_output_$(time).ipynb --log-output"

jemx-spec:
	docker run -e NBARGS_PYDICT="$(NBARGS_PYDICT)" -e ODA_TOKEN="${ODA_TOKEN}" -v $(PWD):/home/jovyan -it --entrypoint='' -p $(JUPYTER_PORT):$(JUPYTER_PORT) --user $(DUSER)  $(IMAGE_LATEST) bash -c "echo \"${NBARGS_PYDICT}\" > nbargs.yaml; cat nbargs.yaml; papermill --parameters_file nbargs.yaml jemx-spectrum.ipynb out/jemx-spectrum_output_$(time).ipynb --log-output"


run-reana:
	reana-client   -lDEBUG   create  -f reana.yaml --name $(WORKFLOW)
	reana-client   -lDEBUG   start --workflow $(WORKFLOW)

prepare-folder:
	mkdir log out rev

clean-notebook:
	jupyter nbconvert --clear-output --inplace *.ipynb

clean: 
	rm -f *.png *.fits *.fits.gz
	rm -f *.reg *.csv *.png
	rm -f *results*.txt
	rm -f statistic*.txt
	rm -f output-*.ipynb
	rm -f api_cat_str_*.txt
	rm -f grb_table_*.txt

clean_output_folders:
	rm -rf output_folder_ProcessNRTRevolutions_*
	rm -rf output_folder_ProcessCONSRevolutions_*

squash: build
	docker build . -t $(IMAGE)-noentry -f Dockerfile-noentry

singularity: squash
	docker run -v /var/run/docker.sock:/var/run/docker.sock -v ${PWD}:/output --privileged -t --rm quay.io/singularity/docker2singularity $(IMAGE)


